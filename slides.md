---
marp: true
title: Slide Title
description: Slide description
keywords: Marp, Slides
theme: gaia
#theme: default
size: 4:3
#size: 16:9
_class: lead
paginate: true
_paginate: false
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
style: |
  table {
    width: 100%;
    margin: 0 auto;
    margin-top: 1em;
    font-size: 0.75em;
  }
  h1 {
    font-size: 1.5em;
  }
  h2 {
    font-size: 1em;
  }
  p {
    font-size: 0.75em;
  }

---

<!--
_class: lead gaia
_paginate: false
-->
# <!-- fit --> Presentation Title

by Adam Kaminski

- [GitHub](https://github.com/thimslugga)
- [Blog](https://adamkaminski.com)

---

<!-- backgroundColor: white -->
## Agenda

- What is Marp?
- Content slide
- What cool features does it have?
- Conclusion

---

## What is Marp?

The [Marp Presentation Ecosystem](https://marp.app/) (Marp for short) is a collection of tools that allow you to create elegant slide decks using the familiar Markdown syntax

---

## Content slide 

- Markdown is great
- Focus on cool stuff and demos and not fighting with PowerPoint
- Use Git, commit and version control

---

## List Slide

- Item 1
- Item 2
- Item 3

---

## Image Slide

![Image](https://picsum.photos/800/600)

---

## Quote Slide

> Split pages by horizontal ruler (`---`). It's so simple! :satisfied:

---

## Table Slide

| Column 1 | Column 2 |
| -------- | -------- |
| Item 1   | Item 2   |
| Item 3   | Item 4   |

---

## Footer Slide

1. Numbered Item 1
2. Numbered Item 2

<!-- _footer: "https://wikipedia.org" -->

---