# Make Presentations with Markdown

## Getting Started

- https://marp.app/
- https://github.com/marp-team/marp

### What is Markdown?

- https://marpit.marp.app/markdown
- https://commonmark.org/help/

### Tools

- https://github.com/marp-team/marp-cli
- https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode
- https://github.com/favourhong/Awesome-Marp
- https://github.com/cupcakearmy/markdown-it-import
- https://github.com/JichouP/obsidian-marp-plugin
- https://github.com/samuele-cozzi/obsidian-marp-slides

### Examples

- https://raw.githubusercontent.com/yhatt/marp-cli-example/master/PITCHME.md
- https://github.com/yhatt/marp-cli-example

### Assets and Themes

- The `assets` directory can put your assets for using in the deck. (e.g. Image resources)
- The `themes` directory can put custom theme CSS. To use in the deck, please change theme global directive.
- The built assets will output to the `public` directory.

- https://marpit.marp.app/theme-css
- https://github.com/marp-team/marp-core/tree/main/themes

#### Community Themes

- https://github.com/matsubara0507/marp-themes
- https://github.com/rainbowflesh/Rose-Pine-For-Marp
- https://github.com/dracula/marp
- https://github.com/cunhapaulo/marpstyle

### Directives

- https://marpit.marp.app/directives

### Images

- https://marpit.marp.app/image-syntax

### Font Awesome

- https://fontawesome.com/

### Mermaid.js

- https://mermaid-js.github.io/mermaid/#/
- https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid
- https://github.com/marp-team/marp-core/issues/139

To use Mermaid diagrams in your Marp presentation, simply add the following script to your Markdown file:

```html
<!-- Add this anywhere in your Markdown file -->
<script type="module">
  import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
  mermaid.initialize({ startOnLoad: true });
</script>
```

```css
  svg[id^="mermaid-"] { 
    min-width: 480px; 
    max-width: 960px; 
    min-height: 360px; 
    max-height: 600px; 
  }
```

```mermaid
<div class="mermaid">
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
</div>
```

## Automating Deployment

- https://github.com/marketplace/actions/marp-action
- https://github.com/yhatt/marp-cli-example/blob/master/.github/workflows/github-pages.yml
- https://github.com/KoharaKazuya/marp-cli-action
- https://github.com/ralexander-phi/marp-to-pages
- https://docs.gitlab.com/ee/user/project/pages/

## Alternatives

- https://news.ycombinator.com/item?id=34501901
- https://obsidian.md/
- https://pandoc.org/
- https://revealjs.com/
- https://quarto.org/