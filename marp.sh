#!/bin/bash

_docker_cmd="${DOCKER_CMD:-docker}"

function mkvenv() {
  local _venv
  _venv="${1}"
  test ! -d ~/.venvs && mkdir -p "${HOME}/.venvs"
  
  printf "creating virtualenv ${_venv} in ~/.venvs.. \n"
  python3 -m venv "${HOME}/.venvs/${_venv}"
  source "${HOME}/.venvs/${_venv}/bin/activate"
  #python -m pip install <package>
}

function podman-marp() { 
  "${_docker_cmd}" run --rm -v "$(pwd)":/home/marp/app/ -e LANG=$LANG marpteam/marp-cli "${*}"
}

# Start the slides server in watch mode
function marp-watch() {
  npx @marp-team/marp-cli@latest -w slides.md
}

# generate a pdf
function marp2pdf() {
  npx @marp-team/marp-cli@latest slides.md -o slides.pdf
}

# generate a pptx
function marp2pptx() {
  npx @marp-team/marp-cli@latest slides.md -o slides.pptx
}